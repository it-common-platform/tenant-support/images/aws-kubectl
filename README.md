# AWS Kubectl

An image that provides both the AWS CLI and [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) tool. Also includes jq. 

## Use Cases

- Running a cron job that obtains ECR credentials and stores them as Kubernetes secrets.



