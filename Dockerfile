FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}bitnami/kubectl:1.30 AS kubectl

FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}amazon/aws-cli:2.15.48
ENTRYPOINT []
COPY --from=kubectl /opt/bitnami/kubectl/bin/kubectl /usr/local/bin/kubectl

# Install jq & kubeseal
# Sealed Secrets releases: https://github.com/bitnami-labs/sealed-secrets/releases
ENV KUBESEAL_VERSION=0.26.2 \
    KUBESEAL_CHECKSUM=9681dd97d245e2cd24af7d2f44c962ddd3e7eb67443f9f8ab3c0633b3b0a15a7
RUN curl -jksSL -o kubeseal.tgz https://github.com/bitnami-labs/sealed-secrets/releases/download/v${KUBESEAL_VERSION}/kubeseal-${KUBESEAL_VERSION}-linux-amd64.tar.gz && \
    echo "${KUBESEAL_CHECKSUM}  ./kubeseal.tgz" | sha256sum -c - && \
    yum -y install jq tar gzip && \
    tar -C /usr/local/bin -zxf ./kubeseal.tgz && \
    yum -y erase tar gzip && \
    yum -y clean all && rm -rf /var/cache
